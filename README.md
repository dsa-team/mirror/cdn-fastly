This code depends on https://github.com/tfheen/fastly-python, you can
install it into your virtualenv like this:

  pip install git+https://github.com/tfheen/fastly-python
