#! /usr/bin/env python3

# Copyright Tollef Fog Heen <tfheen@err.no>, 2016
# Copyright FINN NO AS, 2016-2017
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Configure Fastly according to yaml files.
#
from __future__ import print_function

import sys
import yaml
import logging
import colorlog
import os
import fastly
import glob
import argparse

BASEDIR = os.path.realpath(os.path.join(os.path.dirname(__file__), ".."))

sys.path.append(os.path.join(BASEDIR, "lib"))

print(sys.path)

import fastly_yaml


class PrefixAdapter(logging.LoggerAdapter):
    def __init__(self, prefix, *args, **kwargs):
        self.prefix = prefix
        super(PrefixAdapter, self).__init__(*args, **kwargs)

    def process(self, msg, kwargs):
        return f"{self.prefix} {msg}", kwargs


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--path", default=os.path.join(BASEDIR, "services/*.yaml"), help="Path to config-files")
    parser.add_argument("-l", "--logging", default='info', choices=['info', 'debug'], help="Logging level of app.")
    parser.add_argument("-s", "--syncfastly", action='store_true',
                        help="Sync config with fastly-side. Running without this means you're doing a dry-run.")
    options = parser.parse_args()

    handler = colorlog.StreamHandler()
    handler.setFormatter(colorlog.ColoredFormatter(
        '%(log_color)s%(asctime)s %(levelname)s [%(filename)s:%(lineno)s]:%(message)s'))

    logger = colorlog.getLogger('sync')
    if options.logging.lower() == 'debug':
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
    logger.addHandler(handler)

    client = fastly.connect(os.environ['FASTLY_API_KEY'])

    configs = glob.glob(options.path)
    dryrun = (not options.syncfastly)

    logger.debug("Dryrun mode enabled? " + str(dryrun))

    for conffile in configs:
        conf = yaml.safe_load(open(conffile))
        logger.info("Pushing config file: " + conffile)

        f = fastly_yaml.FastlyConfig(
                client,
                logger=PrefixAdapter(f"[{os.path.basename(conffile)}]:", logger, {}),
                dry_run=dryrun)
        f.update(conf, os.path.abspath(conffile))

if __name__ == '__main__':
    main()
